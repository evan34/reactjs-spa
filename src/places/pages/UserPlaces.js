import React from 'react'
import {useParams} from 'react-router-dom'

import PlaceList from '../components/PlaceList'


const TEMP_PLACES = [
  {
    id: 'p1',
    title: 'azeaze',
    description: 'lorem',
    imageUrl: 'https://picsum.photos/seed/picsum/200/300',
    address: '20 W 34th St, New York, NY 10001',
    location: {
      lat: 40.7484405,
      lng: -73.9878531
    },
    creator: 'u1'
  },
  {
    id: 'p2',
    title: 'azeaze',
    description: 'lorem',
    imageUrl: 'https://picsum.photos/seed/picsum/200/300',
    address: '20 W 34th St, New York, NY 10001',
    location: {
      lat: 40.7484405,
      lng: -73.9878531
    },
    creator: 'u2'
  }
]

const UserPlaces = () => {
  const userId = useParams().userId
  const loadedPlaces = TEMP_PLACES.filter(place => place.creator === userId)
  return <PlaceList items={loadedPlaces} />
}

export default UserPlaces