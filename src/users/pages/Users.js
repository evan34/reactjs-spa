import React from 'react'

import UserList from '../components/UsersList'

const Users = () => {
  const USERS = [{
    id: 'u1',
    image: "https://loremflickr.com/320/240/paris,girl/all",
    name: 'Brendon',
    places: 3
  }]
  return <UserList items={USERS}/>
}

export default Users;
